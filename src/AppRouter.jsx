import React from "react";
import { Route, Routes } from "react-router-dom";
import { HomePage } from "./pages/HomePages";
import {CategoriaPage} from './pages/CategoriaPage'
import { Navbar } from "./components/Navbar";
 const AppRouter =() => {
    return (
        <>
            <Navbar/>             
            <Routes>
                <Route path="/" element={<HomePage/>}/>
                <Route path="/categoria/:categoria" element={<CategoriaPage/>}/>
            </Routes>
        </>
    )
};

export default AppRouter;