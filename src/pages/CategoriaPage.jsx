import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Question } from '../components/Question';
import { questions, imgs } from '../data';

// Función para barajar las preguntas de cada categoría y también reducirla al número de 5
const shuffleArray = array => {
	const newArray = array.sort(() => Math.random() - 0.5);
	return newArray.slice(0, 5);
};

export const CategoriaPage = () => {
	// Leer el parámetro de la URL
	const { categoria } = useParams();

	// Asegúrate de que categoria está definido antes de usarlo
	if (!categoria) {
		return <div>Error: categoría no encontrada</div>;
	}

	const imgCategory = imgs.find(
		img => img === `/src/assets/${categoria.toLowerCase()}.png`
	);

	const [initialQuestionsFiltered, setInitialQuestionsFiltered] = useState(
		questions.filter(question => question.category === categoria)
	);
	const [shuffledQuestions, setShuffledQuestions] = useState([]);
	const [indexQuestion, setIndexQuestion] = useState(0);
	const [activeQuiz, setActiveQuiz] = useState(false);

	useEffect(() => {
		const newQuestions = shuffleArray(initialQuestionsFiltered);
		setShuffledQuestions(newQuestions);
	}, [initialQuestionsFiltered]);

	return (
		<div
			className='container flex flex-col items-center justify-center gap-10'
			style={{ height: 'calc(100vh - 5rem)' }}
		>
			{activeQuiz ? (
				<Question
					filteredQuestion={shuffledQuestions[indexQuestion]}
					setIndexQuestion={setIndexQuestion}
					indexQuestion={indexQuestion}
					questionsFiltered={shuffledQuestions}
					setActiveQuiz={setActiveQuiz}
				/>
			) : (
				<>
					<div className='flex flex-col gap-5'>
						<h1 className='text-3xl text-teal-900 text-center font-bold'>
							{categoria}
						</h1>

						<div className='flex justify-center items-center'>
							{imgCategory ? (
								<img
									src={imgCategory}
									alt={categoria}
									className='w-72'
								/>
							) : (
								<p>Imagen no disponible</p>
							)}
						</div>
					</div>

					<button
						className='text-white bg-gray-900 py-2 rounded-lg font-bold px-5 transition-all hover:bg-yellow-500 hover:text-gray-900'
						onClick={() => setActiveQuiz(true)}
					>
						Iniciar Quiz
					</button>
				</>
			)}
		</div>
	);
};

export default CategoriaPage;
